terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.68.0"
    }
  }
}

provider "yandex" {
  token     = ""
  cloud_id  = "cloud-33pda"
  folder_id = "b1gk2ai7nhqsar0e9k55"
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "vm1" {
  name        = "vm1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "vm1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd83klic6c8gfgi40urb" # Ubuntu 20.04
      size = 20
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.sf-b10.id}"
    ipv4 = "true"
    nat = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("../keys/vm1.pub")}"
    serial-port-enable: "1"
  }
}

resource "yandex_compute_instance" "vm2" {
  name        = "vm2"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "vm2"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd83klic6c8gfgi40urb" # Ubuntu 20.04
      size = 20
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.sf-b10.id}"
    ipv4 = "true"
    nat = "true"
  }
  metadata = {
    ssh-keys = "ubuntu:${file("../keys/vm2.pub")}"
    serial-port-enable: "1"
  }
}

resource "yandex_compute_instance" "vm3" {
  name        = "vm3"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  hostname    = "vm3"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd80s5atj1quloqfr00f" # Centos 8
      size = 20
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.sf-b10.id}"
    ipv4 = "true"
    nat = "true"
  }
  metadata = {
    ssh-keys = "cloud-user:${file("../keys/vm3.pub")}"
    serial-port-enable: "1"
  }
}

resource "yandex_vpc_network" "sf-b10" { }
  
resource "yandex_vpc_subnet" "sf-b10" {
    zone       = "ru-central1-a"
    network_id = "${yandex_vpc_network.sf-b10.id}"
    v4_cidr_blocks = ["172.16.0.0/16"]
}